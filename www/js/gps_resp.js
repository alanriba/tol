
var map = null;
var directionsManager;
var directionsErrorEventObj;
var directionsUpdatedEventObj;


function InitMap() {

    var direccion = JSON.parse(localStorage.getItem('gps_direccion'));
    var lat = direccion.latitud || 33;
    var lon = direccion.longitud || -44;

    map = new  Microsoft.Maps.Map(document.getElementById('divMap'), { 
        credentials: "AvnfdZeoeNR9_ZH_erAdt-ai81ZDOAaQJwzbj7wdTVcuwCJ5BrIj38E9-WZD4lRV",
        mapTypeId:  Microsoft.Maps.MapTypeId.road,
        enableClickableLogo: false,
        enableSearchLogo: false,
        center: new  Microsoft.Maps.Location(lat, lon),
        zoom: 15,
        showDashboard: false
    });
    map2 = new  Microsoft.Maps.Map(document.getElementById('divMap2'), { 
        credentials: "AvnfdZeoeNR9_ZH_erAdt-ai81ZDOAaQJwzbj7wdTVcuwCJ5BrIj38E9-WZD4lRV",
        mapTypeId:  Microsoft.Maps.MapTypeId.road,
        enableClickableLogo: false,
        enableSearchLogo: false,
        center: new  Microsoft.Maps.Location(lat, lon),
        zoom: 15,
        showDashboard: false
    });

    if (!directionsManager) {
        Microsoft.Maps.loadModule('Microsoft.Maps.Directions', { callback: createDrivingRoute });
    }

    Microsoft.Maps.Events.addHandler(directionsManager, 'directionsError', displayError);
}

function GetMap() {

    var h = $(window).height() - 260;
    $("#divMap").css("height", h);
    $("#divMap2").css("height", h);

    var zoom = 15;

    navigator.geolocation.getCurrentPosition(
        function(position) {
            setTimeout((function () {
                
                lat = position.coords.latitude || -33;
                lon = position.coords.longitude || 44;

                localStorage.setItem('gps_latitud', lat);
                localStorage.setItem('gps_longitud', lon);
                
                var direccion = JSON.parse(localStorage.getItem('gps_direccion'));
                var lat_dest = direccion.latitud;
                var lon_dest = direccion.longitud;

                createDrivingRoute();

                // Microsoft.Maps.loadModule('Microsoft.Maps.Themes.BingTheme', { callback: function() {
                //     // setTimeout((function () {
                //     //     viewOptions = {zoom: 15, center: new Microsoft.Maps.Location(lat, lon)};
                //     //     map2.setView(viewOptions);
                //     //     viewOptions = {zoom: 15, center: new Microsoft.Maps.Location(lat_dest, lon_dest)};
                //     //     map.setView(viewOptions);
                //     //     var center = map.getCenter();
                //     //     var pin = new Microsoft.Maps.Pushpin(center, {
                //     //       icon:"images/map-marker.png", 
                //     //       height:50, 
                //     //       width:50, 
                //     //       anchor: new Microsoft.Maps.Point(0,50), draggable: false}); 
                //     //     map.entities.push(pin);
                //     // }).bind(this), 2000);
                //  }
                // });
            }).bind(this), 2000);

            setTimeout((function () {
                
            }).bind(this), 2000);
        },
        function(error) {
            console.log(error);
        }
    );
    // }
    // , function() {
    //   console.log("error gps");
    //   swal("Alerta", "Sin acceso a GPS, por favor activarlo para una mejor experiencia de uso", "warning");
    
}

function createDrivingRoute() {

    setTimeout((function () {  

        watchId = navigator.geolocation.watchPosition(function(position) {

            var lat = position.coords.latitude || -33;
            var lon = position.coords.longitude || 44;

            var direccion = JSON.parse(localStorage.getItem('gps_direccion'));
            var lat_dest = direccion.latitud;
            var lon_dest = direccion.longitude;

            if (!directionsManager) {
                var directionsManager = new Microsoft.Maps.Directions.DirectionsManager(map2);
            } else {
                directionsManager.resetDirections();
                directionsErrorEventObj = Microsoft.Maps.Events.addHandler(directionsManager, 'directionsError', function (arg) { });
                directionsUpdatedEventObj = Microsoft.Maps.Events.addHandler(directionsManager, 'directionsUpdated', function() { });    
            }
            directionsManager.resetDirections();

            directionsManager.setRequestOptions({ routeMode: Microsoft.Maps.Directions.RouteMode.driving });
            var origen = new Microsoft.Maps.Directions.Waypoint({ location: new Microsoft.Maps.Location(lat, lon) });
            directionsManager.addWaypoint(origen);

            var destino = new Microsoft.Maps.Directions.Waypoint({ location: new Microsoft.Maps.Location(lat_dest, lon_dest) });
            directionsManager.addWaypoint(destino);

            // directionsManager.setRenderOptions({ itineraryContainer: document.getElementById('directionsItinerary') });
            directionsManager.calculateDirections();
            
            viewOptions = {zoom: 15, center: new Microsoft.Maps.Location(lat, lon)};
            map2.setView(viewOptions);
            var center = map.getCenter();

            viewOptions = {zoom: 15, center: new Microsoft.Maps.Location(lat_dest, lon_dest)};
            map.setView(viewOptions);
            var center = map.getCenter();
            
            var pin = new Microsoft.Maps.Pushpin(center, {
              icon:"images/map-marker.png", 
              height:50, 
              width:50, 
              anchor: new Microsoft.Maps.Point(0,50), draggable: false}); 
            map.entities.push(pin);

            viewOptions = {zoom: 15, center: new Microsoft.Maps.Location(lat, lon)};
            map2.setView(viewOptions);

            viewOptions = {zoom: 15, center: new Microsoft.Maps.Location(lat_dest, lon_dest)};
            map.setView(viewOptions);
            
            var center = map.getCenter();
            var pin = new Microsoft.Maps.Pushpin(center, {
              icon:"images/map-marker.png", 
              height:50, 
              width:50, 
              anchor: new Microsoft.Maps.Point(0,50), draggable: false}); 
            map2.entities.push(pin);
        });
        
    }).bind(this), 2000);  
}

function actualizaMapa() {
    watchId = navigator.geolocation.watchPosition(UsersLocationUpdated);
}

function UsersLocationUpdated(position) {
    var loc = new Microsoft.Maps.Location(
                position.coords.latitude,
                position.coords.longitude);
}

function displayError(error) {
  swal("No ha sido posible calcular la ruta", error.message, "error");
}

function createDirectionsManager() {
    var displayMessage = "";
    setTimeout((function () {
        if (!directionsManager) {
            var directionsManager = new Microsoft.Maps.Directions.DirectionsManager(map2);
        } else {
            directionsManager.resetDirections();
            directionsErrorEventObj = Microsoft.Maps.Events.addHandler(directionsManager, 'directionsError', function (arg) { });
            directionsUpdatedEventObj = Microsoft.Maps.Events.addHandler(directionsManager, 'directionsUpdated', function() { });    
        }
    }).bind(this), 2000);
}