function validaRUTCELULAR(rut, celular) {
    if (validaRut(rut)){
        localStorage.setItem('userRut', rut);
        if (/^[56789][0-9]{1,10}$/.test(+celular) && celular.toString().length >= 8 && celular.toString().length <= 11){
            localStorage.setItem('numCelular', celular);
            return '';
        }else{
            return 'Celular Inválido';
        }
    } else {
        return 'RUT Incorrecto';
    }
}
 
function validaRUTF12(rutf12, orden) {
    var textPin = rutf12;
    var textPin2 = orden;
    if ((validaRut(rutf12)) ||((/^12[0-9]{9,9}$/.test(+orden) || orden.toString().length > 0 ) && validaF12(orden))){
        localStorage.setItem('userRut', rutf12);
        return '';
    }else{
        return 'N° Orden Incorrecto o RUT Incorrecto';
    }
}

function validaF12(codigo) {
    iDig = codigo.toString().substring(codigo.toString().length -1, codigo.toString().length);
    iSum = 0;
    j = 1;
    for(i = 1; i < 11; i++){
        iSum = iSum + (codigo.toString().substring(i, i-1 ) * j);
        j = (j == 1? 3: 1);
    }
    iRes0 = iSum % 10;
    iRes1 = 10 - iRes0;
    iRes2 = iRes1 % 10;
    return (iRes2 == iDig);
}

function validaCELULAR(celular) {
    if(/^[56789][0-9]{1,10}$/.test(+celular) && celular.toString().length >= 8 && celular.toString().length <= 11){
        localStorage.setItem('numCelular', celular);
        return '';
    }else{
        return 'Celular Inválido';
    }
}

function validaFIJO(fijo) {
    if(/^([3-7]\d{8})|(22\d{7})$/.test(+fijo) && fijo.toString().length == 9) { 
        localStorage.setItem('numFijo', fijo);
        return '';
    }else{
        return 'Número Fijo Inválido';
    }
}

function validaRut (txtRUT) {
    var rut_prueba = "156900000";
    var tmpstr = "";
    var intlargo = txtRUT
    var crut = txtRUT
    var largo = crut.length;

    if (rut_prueba == txtRUT)
        return true; /* # para pruebas: ELIMINAR ESTA LINEA */

    if (!txtRUT || (!txtRUT) || (txtRUT < 2))
        return false;

    for (i = 0; i < crut.length; i++)
        if (crut.charAt(i) != ' ' && crut.charAt(i) != '.' && crut.charAt(i) != '-') {
            tmpstr = tmpstr + crut.charAt(i);
        }

    rut = tmpstr;
    crut = tmpstr;

    largo = crut.length;

    if (largo > 2)
        rut = crut.substring(0, largo - 1);
    else
        rut = crut.charAt(0);
    dv = crut.charAt(largo - 1);

    if (rut == null || dv == null)
        return 0;

    var dvr = '0';
    suma = 0;
    mul = 2;

    for (i = rut.length - 1; i >= 0; i--) {
        suma = suma + rut.charAt(i) * mul;
        if (mul == 7)
            mul = 2;
        else
            mul++;
    }

    res = suma % 11;
    if (res == 1)
        dvr = 'k';
    else if (res == 0)
        dvr = '0';
    else {
        dvi = 11 - res;
        dvr = dvi + "";
    }

    if (dvr != dv.toLowerCase()) {
        return false;
    }
    return true;
}


function msgError(txt) {
    swal({
        title: "Alerta!", 
        text: txt,
        type: "error",
        html: txt
    });
}