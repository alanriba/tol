var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');

        document.addEventListener("online", onOnline, false);
        document.addEventListener("offline", onOffline, false);

        function onOnline() {
            var s = document.getElementById("conectado");
            s.value = "";

            console.log("recupero conexion");
            // actualiza el contenido de ticket si corresponde
            angular.element(document.getElementById('mainControl')).scope().actualiza();
            // actualiza mapa al recuperar conexion
            angular.element(document.getElementById('mainControl')).scope().actualizaMapa();    
        };

        function onOffline() {
            // swal({
            //     title: "Alerta!",
            //     text: "Sin conexión a internet",
            //     type: "warning",
            //     html: true
            // });
            console.log("pierdo conexion");
            var s = document.getElementById("conectado");
            s.value = "desconectado";

            // actualiza el contenido de ticket si corresponde, para mostrarlo offline
            angular.element(document.getElementById('mainControl')).scope().actualiza();
        }

        console.log("gps");
        // navigator.geolocation.getCurrentPosition (
        //     function(position){
        //         console.log("obtengo coordenadas del gps");
        //         var lat = position.coords.latitude;
        //         var lon = position.coords.longitude;
        //         localStorage.setItem("gps_coords", JSON.stringify({lat: lat, lng: lon}));
        //         localStorage.setItem('gps_latitud', position.coords.latitude);
        //         localStorage.setItem('gps_longitud', position.coords.longitude);
        //     },
        //     function(error){
        //         console.log("NO obtengo coordenadas del gps");
        //         console.log(error);
        //         swal("Error!", "Sin acceso a GPS, favor activarlo para una mejor experiencia de uso", "error");
        //     }
        // );

        if (navigator.geolocation) {

            navigator.geolocation.getCurrentPosition(
                function(position) {
                    console.log("obtengo coordenadas del gps");
                    var lat = position.coords.latitude;
                    var lon = position.coords.longitude;
                    localStorage.setItem("gps_coords", JSON.stringify({lat: lat, lng: lon}));
                    localStorage.setItem('gps_latitud', position.coords.latitude);
                    localStorage.setItem('gps_longitud', position.coords.longitude);
                }, 
                function(error) {
                    console.log("NO obtengo coordenadas del gps");
                    console.log(error);
                    swal({
                        title:"Su Turno M&oacute;vil",
                        msg: "Sin acceso a GPS, favor activarlo para una mejor experiencia de uso", 
                        type: "warning",
                        html: true
                    });        
                }
            );
        }else{
            console.log("NO obtengo coordenadas del gps");
            console.log(error);
            swal("Error!", "Sin soporte GPS en su dispositivo", "error");
        }

        /*
            {timeout:5000, enableHighAccuracy: true}
        */

        document.addEventListener("backbutton", handleBackButton, false);

        function handleBackButton(donde) {
            var hash = window.location.hash;
            console.log(hash);
            var dest = "";

            if(hash == "#pideNumero") dest = "#ticket";
            if(hash == "#ticket") dest = "#sucursales";
            if(hash == "#sucursales") dest = "#detalles";
            if(hash == "#detalles") dest = "#servicios";
            if(hash == "#condiciones") dest = "#login";

            if( angular.element(document.getElementById('showBotVolver')).scope().showBotVolver ) {
                if(dest) {
                    $.mobile.pageContainer.pagecontainer("change", dest, {
                        transition: "none",
                        reverse: true
                    });    
                }    
            }
        }

        var notificationOpenedCallback = function(jsonData) {
            console.log('didReceiveRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
        };

        window.plugins.OneSignal
            .startInit("5478232f-4b4e-49fa-abaa-06a0364abaa0", "372623134342")
            .handleNotificationOpened(notificationOpenedCallback)
            .endInit();
        

        // window.plugins.OneSignal.init("5478232f-4b4e-49fa-abaa-06a0364abaa0",
        //                              {googleProjectNumber: "372623134342"},
        //                              notificationOpenedCallback);
        // // window.plugins.OneSignal.setLogLevel({logLevel: 6, visualLevel: 4});
        window.plugins.OneSignal.enableVibrate(true);
        // window.plugins.OneSignal.enableInAppAlertNotification(true);
        // window.plugins.OneSignal.registerForPushNotifications();

        window.plugins.OneSignal.getIds(function(ids) {
            console.log(JSON.stringify(ids));
            localStorage.setItem("push_id", ids.userId);
            $("#push_id").val(ids.userId);
        });

        if(localStorage.getItem('logeado')) {
            if(localStorage.getItem('ticket_actual')) {
                setTimeout(function() {
                    $.mobile.pageContainer.pagecontainer("change", "#ticket", {
                        transition: "none",
                        reverse: false
                    });
                }, 5);
            } else {
                setTimeout(function() {
                    $.mobile.pageContainer.pagecontainer("change", "#servicios", {
                        transition: "none",
                        reverse: false
                    }); 
                }, 5);   
            }  
        } 

        var viewPortHeight = $(window).height();
        var headerHeight = $('div[data-role="header"]').height();
        var footerHeight = $('div[data-role="footer"]').height();
        var contentHeight = viewPortHeight - headerHeight - footerHeight + 100;

        // Set all pages with class="page-content" to be at least contentHeight
        // $('div[class="ui-content"]').css({'min-height': contentHeight + 'px'});
        $("#miNumero").css('min-height', contentHeight + 'px');
        $("#main_ticket").css('min-height', contentHeight + 'px');
        $(".alto_ticket").css('min-height', contentHeight - 10 + 'px');
        $(".col_ticket").css('min-height', contentHeight *0.65 + 'px');
        $(".col_ticket2").css('min-height', contentHeight *0.65 + 'px');

    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};
app.initialize();





